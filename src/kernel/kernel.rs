#![feature(lang_items)]
#![crate_type = "staticlib"]
#![no_std]

#[no_mangle]
pub extern fn kernel_main() {}

#[lang = "eh_personality"] extern fn eh_personality() {}
#[lang = "panic_fmt"] extern fn panic_fmt() {}
