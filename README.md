# piforth readme file

## Motivation

For many simple hardware hacking projects, installing a fully-fledged
Linux Distro onto the Raspberry Pi is complete overkill,
and also has its problems
(for example no realtime execution, per default at least).
On the other hand, no one wants to mess around with linker scripts,
setting up a C-compatible environment in Assembler and cross-compilation just to
let some LEDs blink.

This is, as I believe, the perfect niche for
[Forth](https://en.wikipedia.org/wiki/Forth_%28programming_language%29).
A fully developed Forth System allows you to interactively run program code,
define functions ('words' in Forth-speak),
like your python/whatever-[REPL](https://en.wikipedia.org/wiki/Read%E2%80%93eval%E2%80%93print_loop),
but also to run code developed on your workstation computer in an unsupervised way (think 'autostart').

## Project Goal

The goal is to develop a Forth System for the Raspberry Pi 1 and Raspberry Pi Zero
(the ones with the ARM1176JZF-S processor) in [Rust](https://www.rust-lang.org/).
Why not the Pi 2?
Because it is aimed for low-level control tasks, which don't need a quad-core CPU,
and to make the development and build process easier (only one processor type).
This may change in the future.

It will have the following features (in no specific order):

* Written in [Rust](https://www.rust-lang.org/)
* [Forth 2012](http://forth-standard.org/standard/words) compliance  
  (including at least the floating-point and file access word sets)
* interactive I/O via UART
* interactive I/O via USB (keyboard only) and HDMI
* read configuration files and program code from SD card   
  (and automatically launch programs according to the config)
* a set of extra (Forth-)words for MMIO, most important GPIO, I2C and SPI

What it won't have (at least not planned for now):

* USB support, apart from keyboard input mentioned above
* Ethernet, audio, component video, camera (CSI), (touch) display (DSI) support

## Links

* Official Documentation: [Wiki](https://bitbucket.org/corin33/piforth/wiki/Home)
* Mailinglist: TBD
